---
mainfont: Open Sans 
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "1η Σειρά ασκήσεων στις Βάσεις Δεδομένων"
author:
- "Ελευθέριος Καπελώνης 03116163"
- "Κωνσταντίνος Αγιάννης 03116352"
- "Δημήτριος Τσιαχρήστος 03116048"
---

# E-R μοντέλο

![ER διάγραμμα](er_diagram.svg)

# Παραδοχές

- Το *employee* δεν το συνδέσαμε με άλλα entities γιατί δεν έχει κάποια συσχέτιση, 
είναι απλώς χρήστης της βάσης δεδομένων. 
Το *reminder_date* ενημερώνεται μέσω queries από έναν εργαζόμενο και δεν έχει σχέση με την οντότητα *employee*.

# Σχεσιακό μοντέλο

employee(**id**,name,salary,date_of_hiring,current_contract_number)

book(**ISBN**,title,publication_year,num_of_pages,publisher_name,writer_id,category_name)

category(**category_name**,subcategory_of)

publisher(**name**,year_of_est,address)

writer(**id**,name,date_of_birth)

copy(**id**,position,ISBN)

member(**id**,name,num_of_books_borrowed,date_of_birth,address)

borrow(**borrow_id**,member_id,copy_id,date_of_borrow,latest_return_date,reminder_date,actual_return_date)

