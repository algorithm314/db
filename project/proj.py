# ιδέες για queries
# 1. πόσα βιβλία υπάρχουν ανά κατηγορία
# 2. πόσα βιβλία έχει ο κάθε εκδοτικός οίκος
# 3. βιβλία ποιων κατηγοριών δανείζονται περισσότερο
# 4. ποια βιβλία δανείζονται περισσότερο
# 5. ποια βιβλία έχουν έλλειψη ( διαθέσημα copies = 0)

from flask import Flask,g,render_template,request,flash
import sqlite3
from datetime import date,timedelta

app = Flask(__name__,static_url_path='/static')

DATABASE = 'test.db'

def get_db():
	db = getattr(g, '_database', None)
	if db is None:
		db = g._database = sqlite3.connect(DATABASE)
	db.commit()
	return db

@app.teardown_appcontext
def close_connection(exception):
	db = getattr(g, '_database', None)
	if db is not None:
		db.close()

class output:
	string = ""
	def add(s,str,end='\n'):
		s.string = s.string + str + end
	
	def get(s):
		ret = s.string
		s.string = ""
		return ret

o = output()

def print_result(statement,table,updatable=True):
	cur = get_db().cursor()
	cur.execute(statement)
	names = [description[0] for description in cur.description]
	rows = cur.fetchall()
	o.add('<table id="res">')

	#first print Column names
	o.add('<tr>')
	for i in names[1:]:
		o.add('<th><b>'+str(i).replace("_"," ")+'</b></th>')
		
	if updatable:
		o.add('<th><b>Delete</b></th>')
	o.add('</tr>')

	
	for i in rows[:]:
		o.add('<tr>')

		for j in range(1,1+len(i[1:])):
			if updatable:
				o.add('<td><a href="/update/%s/%d/%s">%s</a></td>'%(table,i[0],names[j],i[j]))
			else:
				o.add('<td>%s</td>'%(i[j]))
		
		if updatable:
			o.add('<td><a href="/delete/%s/%d"><font color="red">X</font></a></td>' % (table,i[0]) )
		o.add('</tr>')
	o.add('</table>')

########### Below is the app ###########

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/query')
def query():
	return app.send_static_file('query.html')

@app.route('/query/list/<table>')
def query_list(table):
	if table in ['book','employee','category','publisher','writer','copy','member','borrow','belongs_to','written_by','reminder']:
		print_result('SELECT rowid,* FROM '+table+';',table)
		return render_template('template.html',main=o.get())
	elif table == 'members_books_borrowed':
		print_result('SELECT rowid,* FROM '+table+';',table,False)
		return render_template('template.html',main=o.get())
	elif table == 'borrows_view':
		print_result('SELECT borrow_id,* FROM '+table+';',table)
		return render_template('template.html',main=o.get())
	else:
		return render_template('template.html',main='<h1>ERROR:invalid table</h1>')

@app.route('/insert')
def insert():
	conn = get_db()
	cur = conn.cursor()
	cur.execute("SELECT category_name FROM category")
	categories = cur.fetchall()
	return render_template('book.html', categories=categories)

@app.route('/update/<table>/<int:rid>/<attribute>/',methods=['POST','GET'])
def update(table,rid,attribute):
	print("method",request.method)
	if request.method == 'POST':
		r = request.form
		conn = get_db()
		conn.set_trace_callback(print)
		cur = conn.cursor()
		try:
			indexid = 'borrow_id' if table == 'borrows_view' else 'rowid'
			cur.execute("UPDATE "+table+" SET "+attribute+" = ? WHERE "+indexid+" = ?",(r['value'],rid,))
			conn.commit()
		except sqlite3.Error:
			return "an error occured"
		return "Update Ok"
	else:
		return render_template('template.html',main="""<center>
<h2>Insert new value</h2>
<form action = "" method = "POST">
<input type = "text" name = "value" />
<input type = "submit" value = "submit" />
</form>
</center>""")


@app.route('/delete/<table>/<int:rowid>')
def delete(table,rowid):
	conn = get_db()
	cur = conn.cursor()
	if table in ['book','employee','category','publisher','writer','copy','member','borrow','belongs_to','written_by','borrows_view','reminder']:
		try:
			indexid = 'borrow_id' if table == 'borrows_view' else 'rowid'
			cur.execute("DELETE FROM "+table+" WHERE +"+indexid+" = "+str(rowid))
		except:
			return "an error occured"
		conn.commit()
		return "OK"
	else:
		return render_template('template.html',main='<h1>ERROR:invalid table</h1>')

@app.route('/insert/book',methods=['POST'])
def insert_book():
	r = request.form
	conn = get_db()
	cur = conn.cursor()
	try:
		cur.execute("INSERT INTO book VALUES(?,?,?,?,?)",(r['ISBN'],r['title'],r['publication_year'],r['num_of_pages'],r['publisher_name']))
		conn.commit()
	except sqlite3.Error:
		return "an error occured"
	return "insertion Ok"

@app.route('/insert/member',methods=['POST'])
def insert_member():
	r = request.form
	conn = get_db()
	cur = conn.cursor()
	try:
		cur.execute("INSERT INTO member(first_name,last_name,date_of_birth,street,street_number,postal_code) VALUES(?,?,?,?,?,?)",(r['first_name'],r['last_name'],r['date_of_birth'],r['street'],r['street_number'],r['postal_code']))
		conn.commit()
	except sqlite3.Error:
		return "an error occured"
	return "insertion Ok"

@app.route('/insert/borrow',methods=['POST'])
def insert_borrow():
	r = request.form
	conn = get_db()
	cur = conn.cursor()
	try:
		cur.execute("INSERT INTO borrow (member_id, ISBN, copy_number, date_of_borrow) VALUES(?,?,?,?)",(r['member_id'],r['isbn'],r['copy_number'],r['date_of_borrow']))
		conn.commit()
	except sqlite3.Error:
		return "an error occured"
	return "insertion Ok"

@app.route('/remind',methods=['POST','GET'])
def remind():
	if request.method == 'POST':
		r = request.form
		conn = get_db()
		cur = conn.cursor()
		try:
			cur.execute("INSERT INTO reminder SELECT ?,borrow_id,date('now') FROM borrow WHERE actual_return_date IS NULL;",(r['value']))
			conn.commit()
		except sqlite3.Error:
			return "Members have been already reminded today"
		return "Members that have borrowed books have been reminded for each book"
	else:
		return render_template('template.html',main="""<center>
<h2>Your employee id</h2>
<form action = "" method = "POST">
<input type = "text" name = "value" />
<input type = "submit" value = "submit" />
</form>
</center>""")

@app.route('/query/books_per_category')
def query_books_per_category():
	conn = get_db()
	cur = conn.cursor()
	print_result("SELECT book.rowid,belongs_to.category_name,COUNT(belongs_to.category_name) AS 'Number of books' FROM book INNER JOIN belongs_to ON book.isbn = belongs_to.isbn GROUP BY belongs_to.category_name;",'book',False)
	return render_template('template.html',main=o.get())

@app.route('/query/books_per_publisher')
def query_books_per_publisher():
	conn = get_db()
	cur = conn.cursor()
	print_result("SELECT rowid,publisher_name,COUNT(publisher_name) AS Number_of_books  FROM book GROUP BY publisher_name ORDER BY Number_of_books DESC",'book',False)
	return render_template('template.html',main=o.get())

@app.route('/query/books_available')
def query_books_available():
	conn = get_db()
	cur = conn.cursor()
	print_result('''SELECT book.rowid,title, books_left FROM 
		(SELECT copy_count-borrow_count AS books_left, copy_isbn FROM 
			(SELECT isbn as borrow_isbn,COUNT(isbn) as borrow_count from borrow GROUP BY ISBN) 
		INNER JOIN 
			(SELECT isbn as copy_isbn,COUNT(isbn) as copy_count from copy GROUP BY ISBN) 
		ON borrow_isbn = copy_isbn) 
	INNER JOIN book 
	ON book.isbn = copy_isbn''','book',False)
	return render_template('template.html',main=o.get())

@app.route('/query/number_of_books_borrowed')
def query_number_of_books_borrowed():
	conn = get_db()
	cur = conn.cursor()
	print_result('''SELECT member.rowid,member.last_name,member.first_name,books FROM 
	(SELECT member_id,count(member_id) AS books 
		FROM (select member_id FROM borrow WHERE actual_return_date IS NULL) GROUP BY member_id HAVING count(member_id) > 0) 
	INNER JOIN member WHERE member.id = member_id;''','book',False)
	return render_template('template.html',main=o.get())

@app.route('/query/stats')
def query_monthly_salary_costs():
	conn = get_db()
	cur = conn.cursor()
	cur.execute("SELECT SUM(salary) FROM employee");
	salary_expenses = cur.fetchone()[0]
	cur.execute("SELECT count() FROM book");
	number_of_books = cur.fetchone()[0]
	cur.execute("SELECT count() FROM member");
	number_of_members = cur.fetchone()[0]
	
	return render_template('template.html',main='''
	<center>
	<b style="color: #1da1f2;">Monthly salary expenses:</b> %d </br>
	<b style="color: #1da1f2;">Number of books:</b> %d </br>
	<b style="color: #1da1f2;">Number of members:</b> %d
	</center>''' % (salary_expenses,number_of_books,number_of_members));

@app.before_first_request
def startup():
	conn = get_db()
	cur = conn.cursor()
	cur.execute('PRAGMA foreign_keys = ON')

	for filename in ['schema.sql', 'views.sql', 'triggers.sql', 'triggers_view.sql']:
		with open(filename) as file:
			script = file.read()
			cur.executescript(script)
	
	conn.commit()
