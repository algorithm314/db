CREATE VIEW IF NOT EXISTS members_books_borrowed(member_id,number_of_books_borrowed) AS
    SELECT member_id, COUNT(*)
    FROM borrow
    WHERE actual_return_date IS NULL
    GROUP BY member_id;

CREATE VIEW IF NOT EXISTS borrows_view(borrow_id,member_id,ISBN,copy_number,date_of_borrow) AS
    SELECT borrow_id,member_id,ISBN,copy_number,date_of_borrow
    FROM borrow