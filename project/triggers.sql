/* a member can borrow either if he has returned all(for some of which the deadline may have expired) or if he hasn't reached the deadline yet
given that the certain copy_number exists(other members have not borrowed all copies of a specific book)*/

CREATE TRIGGER IF NOT EXISTS borrow_update
    BEFORE INSERT ON borrow
FOR EACH ROW
   WHEN ( (EXISTS (SELECT latest_return_date,actual_return_date 
          FROM borrow 
          WHERE member_id=NEW.member_id AND 
          NEW.date_of_borrow > latest_return_date AND
          actual_return_date IS NULL))
          OR
          (EXISTS (SELECT ISBN,copy_number
          FROM borrow
          WHERE ISBN=NEW.ISBN AND
          copy_number=NEW.copy_number AND
          (actual_return_date > NEW.date_of_borrow OR
          actual_return_date IS NULL)))
          OR
          (EXISTS (SELECT *
          FROM members_books_borrowed
          WHERE member_id=NEW.member_id AND
          number_of_books_borrowed >= 5)
          )
          )
   BEGIN
   SELECT RAISE(ABORT,'error');
   END;

CREATE TRIGGER IF NOT EXISTS borrow_set_return_date
    AFTER INSERT ON borrow
FOR EACH ROW
    WHEN(NEW.latest_return_date IS NULL)
    BEGIN
    UPDATE borrow SET latest_return_date = date(date_of_borrow, '+30 day')
    WHERE rowid = NEW.rowid;
    END