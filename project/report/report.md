---
mainfont: Open Sans
documentclass: extarticle
fontsize: 12pt
geometry: margin=1in
papersize: a4
title: "Βάσεις Δεδομένων - Αναφορά για την εξαμηνιαία εργασία 2018-2019"
author:
- "Ελευθέριος Καπελώνης 03116163"
- "Κωνσταντίνος Αγιάννης 03116352"
- "Δημήτριος Τσιαχρήστος 03116048"
---

# i)

# Πλατφόρμα που χρησιμοποιηθήκε

## Διεπαφή Χρήστη

Για την Διεπαφή Χρήστη της εφαρμογής (front-end) χρησιμοποιήθηκαν η HTML 5 και η CSS που είναι και οι επικρατούσες τεχνολογίες για αυτόν τον σκοπό.
Με τη βοήθεια αυτών των τεχνολογιών έγινε εύκολος ο σχεδιασμός των μενού (HTML) και της εμφάνισής τους (CSS).

Κυρίως χρησιμοποιήθηκαν οι φόρμες της HTML που είναι ένας εύκολος τρόπος να αποστέλλουμε δεδομένα στον server με αίτησεις POST προκειμένου να γίνονται οι εισαγωγές στους πίνακες. 
Ακόμη χρησιμοποιήσαμε ευρέως το tag \<table\> της HTML για την εκτύπωση των αποτελεσμάτων των διαφόρων queries ή πινάκων καθώς και το update και delete (σε όσους αυτό είναι δυνατό δηλαδή σε όλους τους πίνακες και στο δεύτερο (updatable) view.
Τα updates γίνονται πατώντας το πεδίο που επιθυμούμε να αλλάξουμε και υποβάλλοντας την καινούρια τιμή ενώ τα deletes με ένα button σε κάθε tuple.
Επομένως στα πλεονεκτήματα των επιλογών αυτών είναι:

- Εύκολη διεπαφή χρήστη χωρίς πολύπλοκα μενού.
- Εύκολη προσθήκη καινούριων χαρακτηριστικών με την HTML.
- Όμορφο UI.

Το μειονέκτημα βέβαια είναι ότι η διεπαφή είναι στατική και λίγο βαρετή. 
Αυτό θα μπορούσε να επιλυθεί με τη χρήση JavaScript για προσθήκη δυναμικού περιεχομένου αλλά αυτό δεν κρίθηκε απαραίτητο για το συγκεκριμένο project.

## Περιβάλλον ανάπτυξης

Για το περιβάλλον ανάπτυξης (back-end) χρησιμοποιήθηκε η βιβλιοθήκη Flask της Python καθώς και το RDBMS SQLite. 

Με την Flask κάναμε δυνατή την εξυπηρέτηση των αιτήσεων POST της HTML επικοινωνόντας με τη βάση.
Η Flask παρέχει ακόμη έναν built-in server και debugger για εύκολο development (ωστόσο για την κυκλοφορία της εφαρμογής πρέπει να επιλεγεί άλλος server κατάλληλος για production).

Η SQLite χρησιμοποιήθηκε επειδή έχει τα ακόλουθα πλεονεκτήματα:

- Είναι ελαφριά, πιάνει λίγο χώρο στο δίσκο και δεν απαιτεί κανένα dependency.
- Έχει καλή απόδοση.
- Παρουσιάζει μεγάλη αξιοπιστία
- Είναι πολύ εύκολη στη χρήστη αφού έχει όσα χαρακτηριστικά όσα χρειάζεται η εφαρμογή μας.

Ωστόσο έχει και κάποια μειονεκτήματα:

- Δεν έχει καλή συμπεριφορά για μεγάλο traffic κατά την εγγραφή στη βάση (που δε μας απασχολεί στην εφαρμογή μας αφού το traffic του συστήματος της βιβλιοθήκης θα είναι μικρό).
- Λείπουν ορισμένα χαρακτηριστικά που υπάρχουν σε άλλα RDBMS (built-in updatable views, διαγραφή ολόκληρων columns από τη βάση κ.α.) και έτσι σε κάποιες ελάχιστες περιπτώσεις χρειάζεται λίγη περισσότερη αναζήτηση και δουλειά για την υλοποίησή τους.

Γενικά πιστεύουμε ότι η SQLite είναι αρκετή για τη συγκεκριμένη εφαρμογή.

## Testing της εφαρμογής

Για το τρέξιμο της εφαρμογής απαιτείται η Python 3 καθώς και η βιβλιοθήκη Flask.
Η εγκατάσταση της Flask (εφόσον είναι εγκατεστημένη η Python) μπορεί να γίνει με την εντολή:
```
pip3 install flask
```

Για την εκκίνηση του server (σε περιβάλλον Linux):
```
./run.sh
```

Τώρα μπορούμε να πλοηγηθούμε στην εφαρμογή στη διεύθυνση `localhost:5000` σε κάποιον browser.

# ii)

## Περιορισμοί (constraints)
Χρησιμοποιήθηκαν διάφοροι τρόποι για την επιβολή των περιορισμών στη βάση.
Συγκεκριμένα:

- *Primary Keys* 

- *Foreign Keys*

Χρησιμοποιήθηκαν κάθε φορά που αναφερόμασταν σε ένα attribute άλλου πίνακα.
Η βάση απαγορεύει την εισαγωγή tuple εάν δεν υπάρχει αντίστοιχη τιμή στον άλλο 
πίνακα για όσα πεδία έχουν οριστεί Foreign Key. 
Επίσης διαγράφει tuples αν διαγραφούν τιμές σε άλλους πίνακες στις οποίες τα
tuples αναφέρονται μέσω foreign keys (`ON DELETE CASCADE`).

- *Checks* 

Πραγματοποιούνται αρκετά checks κατά την εισαγωγή στους πίνακες ώστε να
αποφεύγονται κάποια λάθη στα πεδία τιμών

- *NOT NULL*

Έτσι ώστε να επιβάλλουμε την ύπαρξη τιμών σε κάποια πεδία που κρίνεται απαραίτητο
(όπως όνομα μέλους, τίτλος βιβλίου κλπ)

- *Triggers*

Περιγράφονται αναλυτικά παρακάτω.

## Τα ευρετήρια

```sql
-- B+-tree index because we check the number of books a member has borrowed
CREATE INDEX IF NOT EXISTS borrow_memberid_index ON borrow(member_id);
-- B+-tree index because we check if the copy has been borrowed
CREATE INDEX IF NOT EXISTS borrow_bookid_index ON borrow(ISBN,copy_number);
```

Στη βάση δεδομένων χρησιμοποιήσαμε 2 ευρετήρια στον πίνακα borrow 
καθώς συχνά γίνεται έλεγχος για την εύρεση του 
αριθμού των βιβλίων που έχει δανειστεί ένα συγκεκριμένο μέλος και για τον
αν ένα συγκεκριμένο αντίτυπο το έχουμε δανείσει ήδη. Για το πρώτο ευρετήριο
είναι καλύτερο το B-tree αφού θα εκτελούνται range queries. Καθώς η SQLite 3
δεν έχει hash index χρησιμοποιήσαμε και για το δεύτερο ένα B-tree. Η απόδοση
είναι και πάλι καλύτερη από την περίπτωση που δε θα χρησιμοποιούσαμε καθόλου
ευρετήριο.

1. Mε βάση το Borrow(member_id)
2. Με βάση το Borrow(ISBN,copy_number)


## iii) DDLs

```sql
CREATE TABLE IF NOT EXISTS employee(
id INTEGER PRIMARY KEY AUTOINCREMENT,
first_name TEXT NOT NULL,
last_name TEXT NOT NULL,
salary INTEGER CHECK( salary > 0 ),
date_of_hiring DATE,
current_contract_number INTEGER
);
```

\(Σε κάθε πίνακα με το IF NOT EXISTS δημιουργείται νέο μόνο όταν δεν
υπάρχει ήδη\)

-   Κάθε εργαζόμενος έχει μοναδικό id(primary key) σε ακέραιο
    αριθμό(autoincrement:είναι μοναδικό και για κάθε νέο στοιχείο
    κατασκευάζεται αυτόματα ένα νέο) , ονοματεπώνυμο (not null:δεν
    μπορεί να μην έχει κανένα), μισθό (θετικό ακέραιο), ημερομηνία
    πρόσληψης (σε μορφή date) και αριθμό τρέχουσας σύμβασης.

```sql
CREATE TABLE IF NOT EXISTS publisher(
	name TEXT PRIMARY KEY,
	year_of_est INTEGER CHECK( year_of_est > 0 and year_of_est <= CAST(strftime('%Y',date('now')) AS INTEGER) ),
	street TEXT,
	street_number INTEGER CHECK( street_number > 0 ),
	postal_code INTEGER CHECK ( postal_code > 0 )
);
```

- Κάθε εκδοτικός οίκος  έχει μοναδικό όνομα,έτος ίδρυσης (μεταξύ 0 και τωρινής ημερομηνίας) και διεύθυνση που ορίζεται από την οδό,τον αριθμό(θετικός ακέ) στην οδό και τον ταχυδρομικό κώδικα.

 ```sql
CREATE TABLE IF NOT EXISTS writer (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	first_name TEXT NOT NULL,
	last_name TEXT NOT NULL,
	date_of_birth DATE
);
```

- Κάθε συγγραφέας έχει μοναδικό id(primary key) σε ακέραιο αριθμό(autoincrement:είναι μοναδικό και για κάθε νέο στοιχείο κατασκευάζεται αυτόματα ένα νέο) , ονοματεπώνυμο (not null:δεν μπορεί να μην έχει κανένα) και ημερομηνία γέννησης.
```sql
CREATE TABLE IF NOT EXISTS member (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	first_name TEXT NOT NULL,
	last_name TEXT NOT NULL,
	date_of_birth DATE,
	street TEXT,
	street_number INTEGER CHECK( street_number > 0 ),
	postal_code INTEGER CHECK( postal_code > 0 )
);
```

- Κάθε μέλος έχει μοναδικό αριθμό μέλους,ονοματεπώνυμο,ημερομηνία γέννησης και διεύθυνση(οδός,αριθμός,ΤΚ).
```sql
CREATE TABLE IF NOT EXISTS category(
	category_name TEXT PRIMARY KEY, 
	subcategory_of TEXT,
	FOREIGN KEY (subcategory_of) REFERENCES category(category_name) ON DELETE CASCADE

);
```
- Κάθε κατηγορία έχει  μοναδικό όνομα και είναι υποκατηγορία ακριβώς μίας συγκεκριμένης κατηγορίας (οπότε αν διαγραφεί η τελευταία τότε θα διαγραφεί και η υποκατηγορία).
```sql
CREATE TABLE IF NOT EXISTS book(
	ISBN TEXT PRIMARY KEY,
	title TEXT NOT NULL,
	publication_year INTEGER CHECK( publication_year > 0 and publication_year <= CAST(strftime('%Y',date('now')) AS INTEGER)),
	num_of_pages INTEGER CHECK( num_of_pages > 0 ),
	publisher_name TEXT,
	FOREIGN KEY (publisher_name) REFERENCES publisher(name) ON DELETE CASCADE
);
```
- Κάθε βιβλίο έχει μοναδικό ISBN,έχει υποχρεωτικά κάποιον τίτλο,έτος έκδοσης(0 με τωρινή ημερομηνία),αριθμό σελίδων(θετικό ακέραιο),όνομα εκδότη.
```sql
CREATE TABLE IF NOT EXISTS copy (
	copy_number INTEGER,
	position TEXT,
	ISBN INTEGER,
	PRIMARY KEY (copy_number, ISBN),
	FOREIGN KEY (ISBN) REFERENCES book ON DELETE CASCADE
);
```
- Κάθε αντίτυπο έχει αριθμό αντιτύπου,θέση στη βιβλιοθήκη, ISBN του αντίστοιχου βιβλίου,ο αριθμός του αντιτύπου ορίζει μοναδικά το κάθε αντίτυπο για συγκεκριμένο βιβλίο-ISBN.
```sql
CREATE TABLE IF NOT EXISTS borrow (
	borrow_id INTEGER PRIMARY KEY AUTOINCREMENT,
	member_id INTEGER NOT NULL,
	ISBN TEXT NOT NULL,
	copy_number INTEGER NOT NULL,
	date_of_borrow DATE NOT NULL,
	latest_return_date DATE,
	actual_return_date DATE,
	FOREIGN KEY (member_id) REFERENCES member(id) ON DELETE CASCADE,
	FOREIGN KEY (ISBN, copy_number) REFERENCES copy (ISBN, copy_number) ON DELETE CASCADE
);
```
- Δανεισμός βιβλίων όπου κάθε ξεχωριστός δανεισμός έχει ξεχωριστό αναγνωριστικό δανεισμού,ύστερα κρατάμε το id του μέλους που δανείστηκε,το βιβλίο και το αντίστοιχο αντίτυπο που δανείστηκε,την ημερομηνία δανεισμού,την προθεσμία δανεισμού και την ημερομηνία που τελικά το επέστρεψε(για να ελέγχουμε πόσα έχει δανειστεί και τι χρωστάει).



- B+-tree index because we check the number of books a member has borrowed
```sql
CREATE INDEX IF NOT EXISTS borrow_memberid_index ON borrow(member_id);
```
- B+-tree index because we check if the copy has been borrowed
```sql
CREATE INDEX IF NOT EXISTS borrow_bookid_index ON borrow(ISBN,copy_number);
```

```sql
CREATE TABLE IF NOT EXISTS belongs_to (
	ISBN TEXT,
	category_name TEXT,
	PRIMARY KEY(ISBN, category_name),
	FOREIGN KEY (ISBN) REFERENCES book ON DELETE CASCADE,
	FOREIGN KEY (category_name) REFERENCES category ON DELETE CASCADE
);
```
- Κάθε βιβλίο-ISBN ανήκει σε μια συγκεκριμένη κατηγορία η οποία έχει κάποιο όνομα και ορίζεται μοναδικά από τα βιβλία που έχει και το όνομά της.
```sql
CREATE TABLE IF NOT EXISTS written_by (
	ISBN TEXT,
	writer_id INTEGER,
	PRIMARY KEY(ISBN,writer_id),
	FOREIGN KEY(writer_id) REFERENCES writer ON DELETE CASCADE,
	FOREIGN KEY(ISBN) REFERENCES book ON DELETE CASCADE
);
```
- Κάθε βιβλίο-ISBN γράφεται υποχρεωτικά από κάποιον συγκεκριμένο συγγραφέα-writer_id .
```sql
CREATE TABLE IF NOT EXISTS reminder (
	employee_id INTEGER,
	borrow_id INTEGER,
	reminder_date DATE NOT NULL,
	PRIMARY KEY (employee_id, borrow_id),
	FOREIGN KEY (employee_id) REFERENCES employee(id) ON DELETE CASCADE,
	FOREIGN KEY (borrow_id) REFERENCES borrow(borrow_id) ON DELETE CASCADE
);
```
- Υπενθυμίσεις που στέλνουν οι εργαζόμενοι, για τις οποίες χρειάζονται το αναγνωριστικό του εργαζομένου που στέλνει κάθε φορά στα μέλη,το id του συγκεκριμένου δανεισμού που είχε διεκπεραιωθεί, τη μη κενή ημερομηνία της υπενθύμισης.Κάθε υπενθύμιση ορίζεται μοναδικά από τον εργαζόμενο που τη στέλνει και το id του δανεισμού στην οποία αναφέρεται.

## VIEWS
```sql
CREATE VIEW IF NOT EXISTS
members_books_borrowed(member_id,number_of_books_borrowed) AS
SELECT member_id, COUNT(*)
FROM borrow
WHERE actual_return_date IS NULL
GROUP BY member_id;
```
- **Μη ενημερώσιμη όψη** για το πλήθος των μελών (SELECT member_id,
COUNT(*)) που έχουν δανειστεί βιβλία και δεν τα έχουν επιστρέψει ακόμα
( WHERE actual_return_date IS NULL) . Τα ομαδοποιούμε με βάση το
member_id.
```sql
CREATE VIEW IF NOT EXISTS
borrows_view(borrow_id,member_id,ISBN,copy_number,date_of_borrow)
AS
SELECT borrow_id,member_id,ISBN,copy_number,date_of_borrow
FROM borrow
```
- **Μη ενημερώσιμη όψη** για το view(για να γίνουν ορατά) του borrow_id
(id δεδομένου δανεισμού) , του member_id του μέλους που δανείστηκε, του
ISBN του δανεισμένου βιβλίου, του copy_number(αριθμό του αντιτύπου του
βιβλίου που δανείστηκε) και του date_of_borrow(ημερομηνία δανεισμού).


- (Για ενημερώσιμες όψεις χρησιμοποίηθηκαν triggers για έμμεση ενημέρωση επειδή η sqlite δε διαθέτει ενημερώσιμες όψεις)
```sql
CREATE TRIGGER IF NOT EXISTS view_insert
INSTEAD OF INSERT ON borrows_view
FOR EACH ROW
BEGIN
INSERT INTO borrow(member_id, ISBN, copy_number, date_of_borrow)
VALUES(NEW.member_id, NEW.ISBN, NEW.copy_number,
NEW.date_of_borrow);
END;
```
- **Trigger για ενημερώσιμη όψη** για εισαγωγή νέου στοιχείου δανεισμού μέσω εισαγωγής
νέου μέλους, του νέου βιβλίου που έχει δανειστεί, το ISBN του , τον
αριθμό αντιτύπου του και την ημερομηνία δανεισμού.

```sql
CREATE TRIGGER IF NOT EXISTS view_delete
INSTEAD OF DELETE ON borrows_view
FOR EACH ROW
BEGIN
DELETE FROM borrow
WHERE borrow_id = OLD.borrow_id;
END;
```
- **Trigger για ενημερώσιμη όψη** για διαγραφή συγκεκριμένου δανεισμού μέσω του
borrow_id που τον διασαφηνίζει κάθε φορά ξεχωριστά.
```sql
CREATE TRIGGER IF NOT EXISTS view_update
INSTEAD OF UPDATE ON borrows_view
FOR EACH ROW
BEGIN
UPDATE borrow SET member_id = NEW.member_id, ISBN = NEW.ISBN,
copy_number = NEW.copy_number, date_of_borrow =
NEW.date_of_borrow;
END;
```
- **Trigger για ενημερώσιμη όψη** για ανανέωση δανεισμού κάνοντας τα νέα στοιχεία του
δανεισμού ως τα πλέον ορατά.

## TRIGGERS
```sql
CREATE TRIGGER IF NOT EXISTS borrow_update
BEFORE INSERT ON borrow
FOR EACH ROW
WHEN ( (EXISTS (SELECT latest_return_date,actual_return_date
FROM borrow
WHERE member_id=NEW.member_id AND
NEW.date_of_borrow > latest_return_date AND
actual_return_date IS NULL))
OR
(EXISTS (SELECT ISBN,copy_number
FROM borrow
WHERE ISBN=NEW.ISBN AND
copy_number=NEW.copy_number AND
(actual_return_date > NEW.date_of_borrow OR
actual_return_date IS NULL)))
OR
(EXISTS (SELECT *
FROM members_books_borrowed
WHERE member_id=NEW.member_id AND
number_of_books_borrowed >= 5)
)
)

BEGIN
SELECT RAISE(ABORT,'error');
END;
```
- **Trigger** που πριν εισάγουμε νέο στοιχείο στο δανεισμό να βγάζει error
όταν είτε υπάρχει νέο μέλος που δανείζεται νέο βιβλίο παρ'όλο που έχει
παρέλθει η περίοδος δανεισμού για κάποιο βιβλίο (NEW.date_of_borrow >
latest_return_date), το οποίο μάλιστα δεν έχει επιστραφεί (
actual_return_date IS NULL ) είτε όταν κάθε αντίτυπο ενός δεδομένου
βιβλίου θα επιστραφεί αργότερα από την επιθυμητή ημερομηνία δανεισμού ή
δε θα επιστραφεί καθόλου οπότε δεν είναι διαθέσιμο κανένα αντίτυπο για
εκ νέου δανεισμό είτε υπάρχει νέο μέλος που έχει δανειστεί τουλάχιστον
 5 βιβλία ταυτόχρονα (number_of_books_borrowed >= 5 ).

```sql
CREATE TRIGGER IF NOT EXISTS borrow_set_return_date
AFTER INSERT ON borrow
FOR EACH ROW
WHEN(NEW.latest_return_date IS NULL)
BEGIN
UPDATE borrow SET latest_return_date = date(date_of_borrow, '+30
day')
WHERE rowid = NEW.rowid;
END
```
- **Trigger** που μετά από εισαγωγή στοιχείου στο δανεισμό, όταν η νέα
προθεσμία παράδοσης βιβλίου δεν έχει ακόμα οριστεί
(WHEN(NEW.latest_return_date IS NULL)) να την ανανεώνει με βάση την
ημερομηνία δανεισμού για 30 μέρες μετά από αυτήν.

## Queries

query_books_per_category():
```sql
SELECT
book.rowid,belongs_to.category_name,COUNT(belongs_to.category_name)
AS 'Number of books' FROM book INNER JOIN belongs_to ON
book.isbn = belongs_to.isbn GROUP BY belongs_to.category_name
```

- Ερώτημα που να εμφανίζει το μοναδικό (primary key) id του κάθε
βιβλίου,το όνομα της κατηγορίας που ανήκει και το πλήθος αυτών στη
δεδομένη κατηγορία από τον πίνακα book σε φυσική αντιστοιχία μόνο για
όσα υπάρχουν με αντίστοιχα στοιχεία και στον belongs_to (INNER JOIN
belongs_to) και μόνο όταν υπάρχουν και στους δύο το ίδιο βιβλίο (ON
book.isbn = belongs_to.isbn) με ομαδοποίηση με βάση το όνομα της κάθε
κατηγορίας.

query_books_per_publisher():
```sql
SELECT rowid,publisher_name,COUNT(publisher_name) AS
Number_of_books FROM book GROUP BY publisher_name ORDER BY
Number_of_books DESC
```
- Ερώτημα που να εμφανίζει το μοναδικό (primary key) id του κάθε
βιβλίου,το όνομα του εκδότη που το εκδίδει και το πλήθος των βιβλίων που
εκδίδει ο συγκεκριμένος από τον πίνακα book με ομαδοποίηση με βάση το
όνομα του κάθε εκδότη με σειρά κατά φθίνοντα αριθμό βιβλίων που εκδίδει
ο κάθε εκδότης (ORDER BY Number_of_books DESC).

query_books_available():
```sql
SELECT book.rowid,title, books_left FROM
(SELECT copy_count-borrow_count AS books_left, copy_isbn FROM
(SELECT isbn as borrow_isbn,COUNT(isbn) as borrow_count from
borrow GROUP BY ISBN)
INNER JOIN
(SELECT isbn as copy_isbn,COUNT(isbn) as copy_count from copy
GROUP BY ISBN)
ON borrow_isbn = copy_isbn)
INNER JOIN book
ON book.isbn = copy_isbn
```
- Ερώτημα (nested) που να εμφανίζει το μοναδικό (primary key) id του
κάθε βιβλίου, τον τίτλο του και το πλήθος

query_number_of_books_borrowed():
```sql
SELECT member.rowid,member.last_name,member.first_name,books FROM
(SELECT member_id,count(member_id) AS books
FROM (select member_id FROM borrow WHERE actual_return_date IS
NULL) GROUP BY member_id HAVING count(member_id) > 0)
INNER JOIN member WHERE member.id = member_id;
```
- Ερώτημα (**nested**) που να εμφανίζει το μοναδικό (primary key) id του
κάθε μέλους, το επώνυμο, το όνομά του και το πλήθος των βιβλίων που έχει
δανειστεί (count(member_id) AS books), το οποίο μέλος ωστόσο χρωστάει
κάποιο βιβλίο (**WHERE** actual_return_date IS NULL) με ομαδοποίηση με
βάση το id των μελών τα οποία ακόμα έχουν δανειστεί ήδη τουλάχιστον ένα
βιβλίο (**HAVING** count(member_id) > 0)) με φυσική αντιστοιχία με τα
μέλη στον πίνακα member.

query_monthly_salary_costs():
```sql
SELECT SUM(salary) FROM employee
SELECT count() FROM book
SELECT count() FROM member
```
- Ερωτήματα που εμφανίζουν αντίστοιχα: το άθροισμα των μισθών όλων των
εργαζομένων (από τον πίνακα emploeyee), το πλήθος όλων των βιβλίων που
υπάρχουν στον πίνακα book και το πλήθος όλων των μελών του πίνακα
member.




