# ii)

## Περιορισμοί (constraints)
Χρησιμοποιήθηκαν διάφοροι τρόποι για την επιβολή των περιορισμών στη βάση.
Συγκεκριμένα:

- *Primary Keys* 

- *Foreign Keys*

Χρησιμοποιήθηκαν κάθε φορά που αναφερόμασταν σε ένα attribute άλλου πίνακα.
Η βάση απαγορεύει την εισαγωγή tuple εάν δεν υπάρχει αντίστοιχη τιμή στον άλλο 
πίνακα για όσα πεδία έχουν οριστεί Foreign Key. 
Επίσης διαγράφει tuples αν διαγραφούν τιμές σε άλλους πίνακες στις οποίες τα
tuples αναφέρονται μέσω foreign keys (`ON DELETE CASCADE`).

- *Checks* 

Πραγματοποιούνται αρκετά checks κατά την εισαγωγή στους πίνακες ώστε να
αποφεύγονται κάποια λάθη στα πεδία τιμών

- *NOT NULL*

Έτσι ώστε να επιβάλλουμε την ύπαρξη τιμών σε κάποια πεδία που κρίνεται απαραίτητο
(όπως όνομα μέλους, τίτλος βιβλίου κλπ)

- *Triggers*

Περιγράφονται αναλυτικά παρακάτω.

## Τα ευρετήρια

```sql
-- B+-tree index because we check the number of books a member has borrowed
CREATE INDEX IF NOT EXISTS borrow_memberid_index ON borrow(member_id);
-- B+-tree index because we check if the copy has been borrowed
CREATE INDEX IF NOT EXISTS borrow_bookid_index ON borrow(ISBN,copy_number);
```

Στη βάση δεδομένων χρησιμοποιήσαμε 2 ευρετήρια στον πίνακα borrow 
καθώς συχνά γίνεται έλεγχος για την εύρεση του 
αριθμού των βιβλίων που έχει δανειστεί ένα συγκεκριμένο μέλος και για τον
αν ένα συγκεκριμένο αντίτυπο το έχουμε δανείσει ήδη. Για το πρώτο ευρετήριο
είναι καλύτερο το B-tree αφού θα εκτελούνται range queries. Καθώς η SQLite 3
δεν έχει hash index χρησιμοποιήσαμε και για το δεύτερο ένα B-tree. Η απόδοση
είναι και πάλι καλύτερη από την περίπτωση που δε θα χρησιμοποιούσαμε καθόλου
ευρετήριο.

1. Mε βάση το Borrow(member_id)
2. Με βάση το Borrow(ISBN,copy_number)

## Οι όψεις

```sql
CREATE VIEW IF NOT EXISTS members_books_borrowed(member_id,number_of_books_borrowed) AS
    SELECT member_id, COUNT(*)
    FROM borrow
    WHERE actual_return_date IS NULL
    GROUP BY member_id;

CREATE VIEW IF NOT EXISTS borrows_view(borrow_id,member_id,ISBN,copy_number,date_of_borrow) AS
    SELECT borrow_id,member_id,ISBN,copy_number,date_of_borrow
    FROM borrow
```

### Τα αντίστοιχα triggers της όψης borrow_view έτσι ώστε να είναι ανανεώσιμη καθώς η SQLite3 δεν υποστηρίζει ανανεώσιμες όψεις.

```sql
CREATE TRIGGER IF NOT EXISTS view_insert
INSTEAD OF INSERT ON borrows_view
FOR EACH ROW
BEGIN
INSERT INTO borrow(member_id, ISBN, copy_number, date_of_borrow) 
VALUES(NEW.member_id, NEW.ISBN, NEW.copy_number, NEW.date_of_borrow);
END;

CREATE TRIGGER IF NOT EXISTS view_delete
INSTEAD OF DELETE ON borrows_view
FOR EACH ROW
BEGIN
DELETE FROM borrow
WHERE borrow_id = OLD.borrow_id;
END;

CREATE TRIGGER IF NOT EXISTS view_update
INSTEAD OF UPDATE ON borrows_view
FOR EACH ROW
BEGIN
UPDATE borrow SET member_id = NEW.member_id, ISBN = NEW.ISBN, 
copy_number = NEW.copy_number, date_of_borrow = NEW.date_of_borrow;
END;
``` 

## Τα Triggers

```sql
/* a member can borrow either if he has returned all(for some of which the deadline may have expired) or if he hasn't reached the deadline yet
given that the certain copy_number exists(other members have not borrowed all copies of a specific book)*/

CREATE TRIGGER IF NOT EXISTS borrow_update
    BEFORE INSERT ON borrow
FOR EACH ROW
   WHEN ( (EXISTS (SELECT latest_return_date,actual_return_date 
          FROM borrow 
          WHERE member_id=NEW.member_id AND 
          NEW.date_of_borrow > latest_return_date AND
          actual_return_date IS NULL))
          OR
          (EXISTS (SELECT ISBN,copy_number
          FROM borrow
          WHERE ISBN=NEW.ISBN AND
          copy_number=NEW.copy_number AND
          (actual_return_date > NEW.date_of_borrow OR
          actual_return_date IS NULL)))
          OR
          (EXISTS (SELECT *
          FROM members_books_borrowed
          WHERE member_id=NEW.member_id AND
          number_of_books_borrowed >= 5)
          )
          )
   BEGIN
   SELECT RAISE(ABORT,'error');
   END;

CREATE TRIGGER IF NOT EXISTS borrow_set_return_date
    AFTER INSERT ON borrow
FOR EACH ROW
    WHEN(NEW.latest_return_date IS NULL)
    BEGIN
    UPDATE borrow SET latest_return_date = date(date_of_borrow, '+30 day')
    WHERE rowid = NEW.rowid;
    END
```
