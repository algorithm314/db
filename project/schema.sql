PRAGMA foreign_keys = ON; -- enable foreign keys constraints enforcement

CREATE TABLE IF NOT EXISTS employee(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	first_name TEXT NOT NULL,
	last_name TEXT NOT NULL,
	salary INTEGER CHECK( salary > 0 ),
	date_of_hiring DATE,
	current_contract_number INTEGER
);

CREATE TABLE IF NOT EXISTS publisher(
	name TEXT PRIMARY KEY,
	year_of_est INTEGER CHECK( year_of_est > 0 and year_of_est <= CAST(strftime('%Y',date('now')) AS INTEGER) ),
	street TEXT,
	street_number INTEGER CHECK( street_number > 0 ),
	postal_code INTEGER CHECK ( postal_code > 0 )
);

CREATE TABLE IF NOT EXISTS writer (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	first_name TEXT NOT NULL,
	last_name TEXT NOT NULL,
	date_of_birth DATE
);

CREATE TABLE IF NOT EXISTS member (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	first_name TEXT NOT NULL,
	last_name TEXT NOT NULL,
	date_of_birth DATE,
	street TEXT,
	street_number INTEGER CHECK( street_number > 0 ),
	postal_code INTEGER CHECK( postal_code > 0 )
);

CREATE TABLE IF NOT EXISTS category(
	category_name TEXT PRIMARY KEY, 
	subcategory_of TEXT,
	FOREIGN KEY (subcategory_of) REFERENCES category(category_name) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS book(
	ISBN TEXT PRIMARY KEY,
	title TEXT NOT NULL,
	publication_year INTEGER CHECK( publication_year > 0 and publication_year <= CAST(strftime('%Y',date('now')) AS INTEGER)),
	num_of_pages INTEGER CHECK( num_of_pages > 0 ),
	publisher_name TEXT,
	FOREIGN KEY (publisher_name) REFERENCES publisher(name) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS copy (
	copy_number INTEGER,
	position TEXT,
	ISBN INTEGER,
	PRIMARY KEY (copy_number, ISBN),
	FOREIGN KEY (ISBN) REFERENCES book ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS borrow (
	borrow_id INTEGER PRIMARY KEY AUTOINCREMENT,
	member_id INTEGER NOT NULL,
	ISBN TEXT NOT NULL,
	copy_number INTEGER NOT NULL,
	date_of_borrow DATE NOT NULL,
	latest_return_date DATE,
	actual_return_date DATE,
	FOREIGN KEY (member_id) REFERENCES member(id) ON DELETE CASCADE,
	FOREIGN KEY (ISBN, copy_number) REFERENCES copy (ISBN, copy_number) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS belongs_to (
	ISBN TEXT,
	category_name TEXT,
	PRIMARY KEY(ISBN, category_name),
	FOREIGN KEY (ISBN) REFERENCES book ON DELETE CASCADE,
	FOREIGN KEY (category_name) REFERENCES category ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS written_by (
	ISBN TEXT,
	writer_id INTEGER,
	PRIMARY KEY(ISBN,writer_id),
	FOREIGN KEY(writer_id) REFERENCES writer ON DELETE CASCADE,
	FOREIGN KEY(ISBN) REFERENCES book ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS reminder (
	employee_id INTEGER,
	borrow_id INTEGER,
	reminder_date DATE NOT NULL,
	PRIMARY KEY (employee_id, borrow_id),
	FOREIGN KEY (employee_id) REFERENCES employee(id) ON DELETE CASCADE,
	FOREIGN KEY (borrow_id) REFERENCES borrow(borrow_id) ON DELETE CASCADE
);

-- B+-tree index because we check the number of books a member has borrowed
CREATE INDEX IF NOT EXISTS borrow_memberid_index ON borrow(member_id);
-- B+-tree index because we check if the copy has been borrowed
CREATE INDEX IF NOT EXISTS borrow_bookid_index ON borrow(ISBN,copy_number);
