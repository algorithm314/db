CREATE TRIGGER IF NOT EXISTS view_insert
INSTEAD OF INSERT ON borrows_view
FOR EACH ROW
BEGIN
INSERT INTO borrow(member_id, ISBN, copy_number, date_of_borrow) 
VALUES(NEW.member_id, NEW.ISBN, NEW.copy_number, NEW.date_of_borrow);
END;

CREATE TRIGGER IF NOT EXISTS view_delete
INSTEAD OF DELETE ON borrows_view
FOR EACH ROW
BEGIN
DELETE FROM borrow
WHERE borrow_id = OLD.borrow_id;
END;

CREATE TRIGGER IF NOT EXISTS view_update
INSTEAD OF UPDATE ON borrows_view
FOR EACH ROW
BEGIN
UPDATE borrow SET member_id = NEW.member_id, ISBN = NEW.ISBN, 
copy_number = NEW.copy_number, date_of_borrow = NEW.date_of_borrow;
END;